Sequel::Model.plugin(:schema)
Sequel::Model.raise_on_save_failure = false
loggers = Padrino.env == :development ? [logger] : nil

environment_db_options = YAML.load_file(File.expand_path(File.dirname(__FILE__)) + '/database.yml')[RACK_ENV]

Sequel::Model.db = Sequel.connect(
    adapter:         environment_db_options['adapter'],
    host:            environment_db_options['host'],
    max_connections: environment_db_options['max_connections'],
    database:        environment_db_options['database'],
    user:            environment_db_options['user'],
    password:        environment_db_options['password'],
    loggers:         [logger]
)

Sequel::Model.db.extension(:pagination)