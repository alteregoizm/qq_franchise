require 'mina/deploy'
require 'mina/git'
require 'mina/rbenv'

set :application_name, 'WikiFranchise'
set :domain, '85.10.201.101'
set :deploy_to, ''
set :repository, ''
set :branch, 'master'
set :user, ''
set :shared_dirs, fetch(:shared_dirs, []).push('public', 'tmp', 'log')
set :shared_files, fetch(:shared_files, []).push('config/database.yml', '.env')

task :environment do
  invoke :'rbenv:load'
end

task :setup do
end

desc "Deploys the current version to the server."
task :deploy do
  invoke :'git:ensure_pushed'
  deploy do
    invoke :'git:clone'
    invoke :'deploy:link_shared_paths'
    invoke :'bundle:install'
    invoke :migrate
    invoke :'deploy:cleanup'

    on :launch do
      in_path(fetch(:current_path)) do
        command %{mkdir -p tmp/}
        command %{touch tmp/restart.txt}
      end
    end
  end

end

desc "Run database migrations."
task :migrate => :environment do
  command "echo '-----> Migrating...' && source ~/.zshrc && cd ~/current && RACK_ENV=production bundle exec rake sq:migrate:up"
end

desc "Run remote application."
task :start => :environment do
  command "echo '-----> Starting thin' && source ~/.zshrc && cd ~/current && bundle exec thin -C config/thin.yml -R config.ru start && echo 'Thin started'"
end

desc "Stop remote application."
task :stop => :environment do
  command "echo '-----> Stopping thin' && cd ~/current && bundle exec thin stop && echo 'Thin stopped'"
end

desc "Restart remote application."
task :restart => :environment do
  invoke :stop
  invoke :start
end

desc "Full deploy with restart."
task :dep => :environment do
  invoke :deploy
  invoke :stop
  invoke :start
end
