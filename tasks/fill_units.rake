task :fill_units => :environment do
  10.times do |time|
    unit = ContentUnit.create(title: "title#{time}", text: "text#{time}")
    PageContent.create(title: 'title', text: 'text', slug: "time#{time}", content_unit_id: unit.id)
  end
end


