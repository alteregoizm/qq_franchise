require 'spec_helper'

RSpec.describe PageContent do
  before(:all) {@page_content = PageContent.create(title: 'title', text: 'text', slug: "slug")}
  after(:all) { PageContent.all.each(&:destroy) }

  it "valid attr" do
    expect(@page_content).to be_valid
  end

end
