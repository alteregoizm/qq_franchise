require 'spec_helper'

RSpec.describe UnitType do
  before(:all) {@unit_type = UnitType.create(name: "name")}
  after(:all) { UnitType.all.each(&:destroy) }

  it "valid attr" do
    expect(@unit_type).to be_valid
  end
end
