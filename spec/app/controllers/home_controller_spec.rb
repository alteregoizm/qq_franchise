require 'spec_helper'

RSpec.describe "/home" do
  before(:all) do
    @unit_type = UnitType.create(name: "name")
    10.times do |time|
      @unit = ContentUnit.create(title: "title#{time}", text: "text#{time}", unit_type_id: @unit_type.id)
      PageContent.create(title: 'title', text: 'text', slug: "time#{time}", content_unit_id: @unit.id)
    end
  end

  after(:all) { PageContent.all.each(&:destroy) }
  after(:all) { ContentUnit.all.each(&:destroy) }
  after(:all) { UnitType.all.each(&:destroy) }

  it 'render index' do
    get '/'
    expect(last_response.status).to eq 200
    expect(last_response.content_type).to include('html')
  end

  it 'render show page with slug' do
    @page_content = PageContent.first
    @content_units = ContentUnit.limit(5)
    get "/#{@page_content.slug}",  slug: @page_content.slug
    expect(last_response.status).to eq 200
    expect(last_response.content_type).to include('html')
  end

  it 'get_contacts' do
    post '/get_contacts', params = {"unit-id" => "#{@unit.id}", "phone" => "111", "email" => "1@1.ru"}
  end
end
