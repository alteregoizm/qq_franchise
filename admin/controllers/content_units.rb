QqFranchise::Admin.controllers :content_units do
  get :index do
    @title = "Content_units"
    @content_units = ContentUnit.all
    render 'content_units/index'
  end

  get :new do
    @title = pat(:new_title, :model => 'content_unit')
    @content_unit = ContentUnit.new
    render 'content_units/new'
  end

  post :create do
    @content_unit = ContentUnit.new(params[:content_unit])
    if (@content_unit.save rescue false)
      @title = pat(:create_title, :model => "content_unit #{@content_unit.id}")
      flash[:success] = pat(:create_success, :model => 'ContentUnit')
      params[:save_and_continue] ? redirect(url(:content_units, :index)) : redirect(url(:content_units, :edit, :id => @content_unit.id))
    else
      @title = pat(:create_title, :model => 'content_unit')
      flash.now[:error] = pat(:create_error, :model => 'content_unit')
      render 'content_units/new'
    end
  end

  get :edit, :with => :id do
    @title = pat(:edit_title, :model => "content_unit #{params[:id]}")
    @content_unit = ContentUnit[params[:id]]
    if @content_unit
      render 'content_units/edit'
    else
      flash[:warning] = pat(:create_error, :model => 'content_unit', :id => "#{params[:id]}")
      halt 404
    end
  end

  put :update, :with => :id do
    @title = pat(:update_title, :model => "content_unit #{params[:id]}")
    @content_unit = ContentUnit[params[:id]]
    if @content_unit
      if @content_unit.modified! && @content_unit.update(params[:content_unit])
        flash[:success] = pat(:update_success, :model => 'Content_unit', :id =>  "#{params[:id]}")
        params[:save_and_continue] ?
          redirect(url(:content_units, :index)) :
          redirect(url(:content_units, :edit, :id => @content_unit.id))
      else
        flash.now[:error] = pat(:update_error, :model => 'content_unit')
        render 'content_units/edit'
      end
    else
      flash[:warning] = pat(:update_warning, :model => 'content_unit', :id => "#{params[:id]}")
      halt 404
    end
  end

  delete :destroy, :with => :id do
    @title = "Content_units"
    content_unit = ContentUnit[params[:id]]
    if content_unit
      if content_unit.destroy
        flash[:success] = pat(:delete_success, :model => 'Content_unit', :id => "#{params[:id]}")
      else
        flash[:error] = pat(:delete_error, :model => 'content_unit')
      end
      redirect url(:content_units, :index)
    else
      flash[:warning] = pat(:delete_warning, :model => 'content_unit', :id => "#{params[:id]}")
      halt 404
    end
  end

  delete :destroy_many do
    @title = "Content_units"
    unless params[:content_unit_ids]
      flash[:error] = pat(:destroy_many_error, :model => 'content_unit')
      redirect(url(:content_units, :index))
    end
    ids = params[:content_unit_ids].split(',').map(&:strip)
    content_units = ContentUnit.where(:id => ids)
    
    if content_units.destroy
    
      flash[:success] = pat(:destroy_many_success, :model => 'Content_units', :ids => "#{ids.join(', ')}")
    end
    redirect url(:content_units, :index)
  end
end
