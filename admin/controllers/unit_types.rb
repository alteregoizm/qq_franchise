QqFranchise::Admin.controllers :unit_types do
  get :index do
    @title = "Unit_types"
    @unit_types = UnitType.all
    render 'unit_types/index'
  end

  get :new do
    @title = pat(:new_title, :model => 'unit_type')
    @unit_type = UnitType.new
    render 'unit_types/new'
  end

  post :create do
    @unit_type = UnitType.new(params[:unit_type])
    if (@unit_type.save rescue false)
      @title = pat(:create_title, :model => "unit_type #{@unit_type.id}")
      flash[:success] = pat(:create_success, :model => 'UnitType')
      params[:save_and_continue] ? redirect(url(:unit_types, :index)) : redirect(url(:unit_types, :edit, :id => @unit_type.id))
    else
      @title = pat(:create_title, :model => 'unit_type')
      flash.now[:error] = pat(:create_error, :model => 'unit_type')
      render 'unit_types/new'
    end
  end

  get :edit, :with => :id do
    @title = pat(:edit_title, :model => "unit_type #{params[:id]}")
    @unit_type = UnitType[params[:id]]
    if @unit_type
      render 'unit_types/edit'
    else
      flash[:warning] = pat(:create_error, :model => 'unit_type', :id => "#{params[:id]}")
      halt 404
    end
  end

  put :update, :with => :id do
    @title = pat(:update_title, :model => "unit_type #{params[:id]}")
    @unit_type = UnitType[params[:id]]
    if @unit_type
      if @unit_type.modified! && @unit_type.update(params[:unit_type])
        flash[:success] = pat(:update_success, :model => 'Unit_type', :id =>  "#{params[:id]}")
        params[:save_and_continue] ?
          redirect(url(:unit_types, :index)) :
          redirect(url(:unit_types, :edit, :id => @unit_type.id))
      else
        flash.now[:error] = pat(:update_error, :model => 'unit_type')
        render 'unit_types/edit'
      end
    else
      flash[:warning] = pat(:update_warning, :model => 'unit_type', :id => "#{params[:id]}")
      halt 404
    end
  end

  delete :destroy, :with => :id do
    @title = "Unit_types"
    unit_type = UnitType[params[:id]]
    if unit_type
      if unit_type.destroy
        flash[:success] = pat(:delete_success, :model => 'Unit_type', :id => "#{params[:id]}")
      else
        flash[:error] = pat(:delete_error, :model => 'unit_type')
      end
      redirect url(:unit_types, :index)
    else
      flash[:warning] = pat(:delete_warning, :model => 'unit_type', :id => "#{params[:id]}")
      halt 404
    end
  end

  delete :destroy_many do
    @title = "Unit_types"
    unless params[:unit_type_ids]
      flash[:error] = pat(:destroy_many_error, :model => 'unit_type')
      redirect(url(:unit_types, :index))
    end
    ids = params[:unit_type_ids].split(',').map(&:strip)
    unit_types = UnitType.where(:id => ids)
    
    if unit_types.destroy
    
      flash[:success] = pat(:destroy_many_success, :model => 'Unit_types', :ids => "#{ids.join(', ')}")
    end
    redirect url(:unit_types, :index)
  end
end
