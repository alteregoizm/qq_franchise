QqFranchise::Admin.controllers :page_contents do
  get :index do
    @title = "Page_contents"
    @page_contents = PageContent.all
    render 'page_contents/index'
  end

  get :new do
    @title = pat(:new_title, :model => 'page_content')
    @page_content = PageContent.new
    render 'page_contents/new'
  end

  post :create do
    @page_content = PageContent.new(params[:page_content])
    if (@page_content.save rescue false)
      @title = pat(:create_title, :model => "page_content #{@page_content.id}")
      flash[:success] = pat(:create_success, :model => 'PageContent')
      params[:save_and_continue] ? redirect(url(:page_contents, :index)) : redirect(url(:page_contents, :edit, :id => @page_content.id))
    else
      @title = pat(:create_title, :model => 'page_content')
      flash.now[:error] = pat(:create_error, :model => 'page_content')
      render 'page_contents/new'
    end
  end

  get :edit, :with => :id do
    @title = pat(:edit_title, :model => "page_content #{params[:id]}")
    @page_content = PageContent[params[:id]]
    if @page_content
      render 'page_contents/edit'
    else
      flash[:warning] = pat(:create_error, :model => 'page_content', :id => "#{params[:id]}")
      halt 404
    end
  end

  put :update, :with => :id do
    @title = pat(:update_title, :model => "page_content #{params[:id]}")
    @page_content = PageContent[params[:id]]
    if @page_content
      if @page_content.modified! && @page_content.update(params[:page_content])
        flash[:success] = pat(:update_success, :model => 'Page_content', :id =>  "#{params[:id]}")
        params[:save_and_continue] ?
          redirect(url(:page_contents, :index)) :
          redirect(url(:page_contents, :edit, :id => @page_content.id))
      else
        flash.now[:error] = pat(:update_error, :model => 'page_content')
        render 'page_contents/edit'
      end
    else
      flash[:warning] = pat(:update_warning, :model => 'page_content', :id => "#{params[:id]}")
      halt 404
    end
  end

  delete :destroy, :with => :id do
    @title = "Page_contents"
    page_content = PageContent[params[:id]]
    if page_content
      if page_content.destroy
        flash[:success] = pat(:delete_success, :model => 'Page_content', :id => "#{params[:id]}")
      else
        flash[:error] = pat(:delete_error, :model => 'page_content')
      end
      redirect url(:page_contents, :index)
    else
      flash[:warning] = pat(:delete_warning, :model => 'page_content', :id => "#{params[:id]}")
      halt 404
    end
  end

  delete :destroy_many do
    @title = "Page_contents"
    unless params[:page_content_ids]
      flash[:error] = pat(:destroy_many_error, :model => 'page_content')
      redirect(url(:page_contents, :index))
    end
    ids = params[:page_content_ids].split(',').map(&:strip)
    page_contents = PageContent.where(:id => ids)
    
    if page_contents.destroy
    
      flash[:success] = pat(:destroy_many_success, :model => 'Page_contents', :ids => "#{ids.join(', ')}")
    end
    redirect url(:page_contents, :index)
  end
end
