class PageContent < Sequel::Model

  plugin :validation_helpers

  mount_uploader :image, ImageUploader

  many_to_one :content_unit

  def validate
    validates_presence :slug
    validates_unique :slug
  end
end
