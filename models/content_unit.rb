class ContentUnit < Sequel::Model
  mount_uploader :image, ImageUploader

  one_to_one :page_content
  many_to_one :unit_type
end
