class UnitType < Sequel::Model
  plugin :validation_helpers

  one_to_many :content_unit

  def validate
    validates_presence :name
    validates_unique :name
  end
end
