$(function () {

  if ($(window).width() < 767) {
    $('.ask-mobile-slider').slick({
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 1,
      arrows: false
    })
  }

   //paralax init
   var rellax = new Rellax('.rellax');
   
   //global phone input init
   $("input[type='tel']").mask("+7 (999) 999-99-99");

   //global scroll elements class
   $('.scroll-na-element').on('click', function(){
      $.scrollTo($('.' + $(this).data('nav')) ,300);
   });

   //global fancy box youtube video open clas
   $(".video-link-popup").click(function() {
    $.fancybox({
      'padding'       : 0,
      'autoScale'     : false,
      'transitionIn'  : 'none',
      'transitionOut' : 'none',
      'title'         : this.title,
      'width'     : 680,
      'height'        : 495,
      'href'          : this.href.replace(new RegExp("watch\\?v=", "i"), 'v/') + '?rel=0&autoplay=1',
      'type'          : 'swf',
      'swf'           : {
        'wmode'        : 'transparent',
        'allowfullscreen'   : 'true'
      }
    });
    return false;
  });

   //.ask-question__block
  $('.ask-question__block').on('click', function(){
    $('.ask-question__block').removeClass('active');
    $(this).addClass('active')
    $('.ask-question__expert').val($(this).find('h4').html())
  });


  function NumberAnimation (id, time, increasable, simbol) {
    var hint = $.animateNumber.numberStepFactories.separator(',');
    var hint_h1_Number = 0;
    if (increasable) {
      var initNumber = parseInt($('#' + id).html().replace(/,/g,'').replace(/ /g,''));

      var Seconds_Between_Dates = getSecondsFromDate();
      
      hint_h1_Number = initNumber + (Seconds_Between_Dates * increasable);
      hint_h1_Number = hint_h1_Number.toFixed();

    } else {
      hint_h1_Number = parseInt($('#' + id).html().replace(/,/g,'').replace(/ /g,''));
    }
    if (simbol) {
      $('#' + id).animateNumber(
        {
          number:  hint_h1_Number,
          numberStep: hint,
          numberStep: function(now, tween) {
            var floored_number = Math.floor(now),
            target = $(tween.elem);
            target.text(floored_number + ' ' + simbol);
          }
        }, 1000
      );
    } else {
      $('#' + id).animateNumber(
        {
          number:  hint_h1_Number,
          numberStep: hint
        }, 1000
      );
    }
  } 

  if ($('.numbers-section').length) {
    $(window).on('scroll', function(){
      if ($(this).scrollTop() >= $('.numbers-section').position().top - 300) {
        NumberAnimation('numbers-section__block-1', 4000);
        NumberAnimation('numbers-section__block-2', 4000);
        NumberAnimation('numbers-section__block-3', 4000);
        NumberAnimation('numbers-section__block-4', 4000, false, '+');
        $(window).off('scroll');
      }
    });
  }
  

  // get-scenario modal
  if ($('.get-scenario-popup').length) {
    var getScenario = new tingle.modal({
      footer: false,
      stickyFooter: false,
      closeLabel: "Close",
      cssClass: ['get-scenario-modal', 'custom-modal'],
      onOpen: function() {
        $("input[type='tel']").mask("+7 (999) 999-99-99");
        $('.popup__form-tel').focus();
      },
      onClose: function() {

      }
    });

    // set content
    getScenario.setContent($('.get-scenario-popup').html());
    $('.get-scenario-popup').html('')

    //get-scenario show button
    $('.get-scenario-popup-show').on('click', function(){
      getScenario.open();
    });

    $('.ask-for-contacts').on('click', function(){
        var unitId = $(this).data('unit');
        $('#ask-for-contact-unit-id').val(unitId);
        getScenario.open();
    });

    $('.get-contacts-popup')
  }

  // thank-you-scenario modal
  if ($('.thank-you-scenario-popup').length) {
    var thankYouScenario = new tingle.modal({
      footer: false,
      stickyFooter: false,
      closeLabel: "Close",
      cssClass: ['thank-you-scenario-modal', 'custom-modal'],
      onOpen: function() {
        // $('.site-search__input').focus();
      },
      onClose: function() {

      }
    });

    // set content
    thankYouScenario.setContent($('.thank-you-scenario-popup').html());
    $('.thank-you-scenario-popup').html('');

    $('.thank-you-30-scenarios').on('click', function(){
      var unitId = $(this).data('unit');
      $('#thank-you-30-scenarios-unit-id').val(unitId);
      thankYouScenario.open();
    });

    //thank-you-scenario show button
    $('.thank-you-scenario-popup-show').on('click', function(){
      thankYouScenario.open();
    });
  }

  // thank-you-guest modal
  if ($('.thank-you-guest-popup').length) {
    var thankYouGuest = new tingle.modal({
      footer: false,
      stickyFooter: false,
      closeLabel: "Close",
      cssClass: ['thank-you-guest-modal', 'custom-modal'],
      onOpen: function() {
        // $('.site-search__input').focus();
      },
      onClose: function() {

      }
    });

    // set content
    thankYouGuest.setContent($('.thank-you-guest-popup').html());
    $('.thank-you-guest-popup').html('');

    $('.guest').on('click', function(){
      var unitId = $(this).data('unit');
      $('#guest-unit-id').val(unitId);
      thankYouGuest.open();
    });

    //thank-you-guest show button
    $('.thank-you-guest-popup-show').on('click', function(){
      thankYouGuest.open();
    });
  }

  // analiz-popup modal
  if ($('.analiz-popup').length) {
    var analizModal = new tingle.modal({
      footer: false,
      stickyFooter: false,
      closeLabel: "Close",
      cssClass: ['get-scenario-modal', 'analiz-modal', 'custom-modal'],
      onOpen: function() {
        $("input[type='tel']").mask("+7 (999) 999-99-99");
        $('.popup__form-tel').focus();
      },
      onClose: function() {

      }
    });

    // set content
    analizModal.setContent($('.analiz-popup').html());
    $('.analiz-popup').html('');

    $('.analiz').on('click', function(){
      var unitId = $(this).data('unit');
      $('#analiz-unit-id').val(unitId);
      analizModal.open();
    });

    //analiz-popup show button
    $('.analiz-popup-show').on('click', function(){
      analizModal.open();
    });
  }

  //overlay-access-hide
    $('.overlay-access-hide').on('click', function(e){
      e.preventDefault();
      $('.overlay-access').fadeOut(300).removeClass('show');
    })

  

});