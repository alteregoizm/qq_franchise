class NotificationJob
  include SuckerPunch::Job

  def perform(inform)
    Curl.get(ENV['CONTACT_SHARING_URL'], { id: ENV['CONTACT_SHARING_CHAT_ID'], text: inform })
  end
end
