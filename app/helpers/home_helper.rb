# Helper methods defined here can be accessed in any controller or view in the application

module QqFranchise
  class App
    module HomeHelper
      def get_unit(position)
        @content_units[position].present? ? @content_units[position] : nil
      end

      def get_ranged_units(range)
        @content_units.slice(range).present? ? @content_units.slice(range) : []
      end
    end

    helpers HomeHelper
  end
end
