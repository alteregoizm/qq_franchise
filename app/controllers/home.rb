QqFranchise::App.controllers :home do
  
  get :index, map: '/' do
    @content_units = ContentUnit.take(9)
    @request_cookies = request.cookies
    render 'index'
  end

  get :show, map: '/', with: :slug do
    @content_units = ContentUnit.limit(5)
    @page_content = PageContent[slug: params[:slug]]
    render 'page'
  end

  post :get_contacts, map: '/get_contacts' do
    unit = ContentUnit[params[:'unit-id']]
    response.set_cookie( unit.unit_type.name, {
        value: 'value_of_cookie',
        domain: request.host,
        path: '/',
        expires: 10.years.from_now
    })
    inform = params[:phone] + ' ' + params[:email]
    NotificationJob.perform_async(inform)
    if unit.page_content.present?
      redirect url(:home, :show, slug: unit.page_content.slug)
    else
      redirect url(:home, :index)
    end
  end

  post :client_request, map: '/client_request' do
    inform = params[:phone] + ' ' + params[:get_details]
    NotificationJob.perform_async(inform)
    redirect url(:home, :index)
  end
end
