Sequel.migration do
  up do
    create_table :page_contents do
      primary_key :id
      integer :content_unit_id, index: true
      String :title
      String :slug, index: true
      String :image
      Text   :text
    end
  end

  down do
    drop_table :page_contents
  end
end
