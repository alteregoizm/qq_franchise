Sequel.migration do
  up do
    create_table :unit_types do
      primary_key :id
      String :name
      String :ru_name
    end
  end

  down do
    drop_table :unit_types
  end
end
