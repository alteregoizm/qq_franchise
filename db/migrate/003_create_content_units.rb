Sequel.migration do
  up do
    create_table :content_units do
      primary_key :id
      String :title
      String :image
      Text   :text
      Integer :unit_type_id, index: true
    end
  end

  down do
    drop_table :content_units
  end
end
